package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Date;

@Component("")
@Order(value = 2)
/**
 * classe interface entre AddBadge Dialog & DigitalBadge
 */
public class DisplayedBadgeHolder {

    private Object metadate;
    private File badge;
    private String serial;
    private Date begin;
    private Date end;

    public DisplayedBadgeHolder(String codeSerieStr, Date debut, Date fin, Object o, File image) {
        this.badge = image;
        this.serial = codeSerieStr;
        this.metadate = o;
        this.begin = debut;
        this.end = fin;
    }

    public DigitalBadge getDisplayedBadge() {
        return displayedBadge;
    }

    public void setDisplayedBadge(DigitalBadge displayedBadge) {
        this.displayedBadge = displayedBadge;
    }

    @Autowired
    private DigitalBadge displayedBadge;

    public DisplayedBadgeHolder setDisplayedBadge(String codeSerieStr, Date debut, Date fin, Object o, File image) {
    return new DisplayedBadgeHolder(codeSerieStr, debut, fin, o, image);
    }




    // TODO: 16/12/2022


}
