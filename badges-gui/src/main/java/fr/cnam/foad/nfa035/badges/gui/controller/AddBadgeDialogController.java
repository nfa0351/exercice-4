package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("")
@Order(value = 2)
public class AddBadgeDialogController {

    @Autowired
    private DirectAccessBadgeWalletDAO dao;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;


    public void delegateOnOK() {
        // TODO: 16/12/2022
    }

    public void delegateForm() {
        // TODO: 16/12/2022
    }



}
