package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * classe permettant l'ajout de badge via linterface graphique
 */
@Component("")
@Order(value = 2)
public class AddBadgeDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JFileChooser fileChooser;

    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;

    //private DigitalBadge badge;

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }
    private DirectAccessBadgeWalletDAO dao;

    public void setCaller(BadgeWalletGUI caller) {
        this.caller = caller;
    }

    @Autowired
    private BadgeWalletGUI caller;

    public AddBadgeDialog() {

        fileChooser.setFileFilter(new FileNameExtensionFilter("Petites images *.jpg,*.png,*.jpeg,*.gif", "jpg","png","jpeg","gif"));
        fileChooser.addPropertyChangeListener(this);
        codeSerie.addPropertyChangeListener(this);
        dateDebut.addPropertyChangeListener(this);
        dateFin.addPropertyChangeListener(this);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Commentez-moi
     */
    private void onOK() {
        try {
            //dao.addBadge(badge);
            //caller.setAddedBadge(badge);
            dao.addBadge(displayedBadgeHolder.getDisplayedBadge());
            caller.setAddedBadge(displayedBadgeHolder.getDisplayedBadge());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        dispose();
    }

    /**
     * Commentez-moi
     */
    private void onCancel() {
        dispose();
    }

    /**
     * Commentez-moi
     * @param args
     */
    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setIconImage(dialog.getIcon().getImage());
        dialog.setVisible(true);
        System.exit(0);
    }

    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * {@inheritDoc}
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (validateForm()){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }
    }

    /**
     * Valide si les champs sélectionnées suffise à la constitution d'un badge
     * @return
     */
    private boolean validateForm(){
        File image = fileChooser.getSelectedFile();
        String codeSerieStr = codeSerie.getText();
        Date fin = dateFin.getDate();
        Date debut = dateDebut.getDate();
        if (codeSerieStr != null && codeSerieStr.trim().length() > 4 && image != null && image.exists()
                && ((fin != null && debut == null)||(fin != null && fin.after(debut)))){
            //this.badge = new DigitalBadge(codeSerieStr,debut,fin,null,image);
            this.displayedBadgeHolder = new DisplayedBadgeHolder(codeSerieStr,debut,fin,null,image);
            return true;
        }
        else{
            return false;
        }
    }


}
