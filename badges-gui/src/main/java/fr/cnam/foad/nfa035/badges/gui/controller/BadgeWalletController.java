package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanelFactory;
import fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("")
@Order(value = 2)
public class BadgeWalletController {

    @Autowired
    private DirectAccessBadgeWalletDAO dao;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;

    @Autowired
    private BadgePanelFactory badgePanelFactory;

    public void delegateUIComponentsCreation(BadgeWalletGUI badgeWalletGUI) {
        // TODO: 16/12/2022
    }

    public void delegateUIManagedFieldsCreation(BadgeWalletGUI badgeWalletGUI) {
        // TODO: 16/12/2022
    }





    
}
