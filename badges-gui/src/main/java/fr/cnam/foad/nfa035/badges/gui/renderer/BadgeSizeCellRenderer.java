package fr.cnam.foad.nfa035.badges.gui.renderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class BadgeSizeCellRenderer extends DefaultTableCellRenderer {

    Color originBackground = null;
    Color originForeground = null;

    public BadgeSizeCellRenderer() {
        super();
        this.originBackground = this.getBackground();
        this.originForeground = this.getForeground();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

        Long size = (Long) value;
        setText(humanReadableByteCountBin(size));
        if (!isSelected) {
            setForeground(this.originBackground);
            if (size > 10000) {
                setBackground(Color.ORANGE);
            } else {
                setBackground(this.originBackground);
            }
        }
        else{
            if (size > 10000) {
                setForeground(Color.RED);
            } else {
                setForeground(this.originBackground);
            }
        }

        return comp;
    }

    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }
}