package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.*;

import static fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider.*;

/**
 * Classe principale de l'interface graphique
 */
@Component("badgeWallet")
@Order(value = 2)
public class BadgeWalletGUI{

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;
    @Autowired
    private BadgePanel badgePanel;
    @Autowired
    private BadgesModel tableModel;
    @Autowired
    private BadgeWalletController badgesWalletController;

    /**
     * aggrégation vers objet DigitalBadge pour récupére méthode badge
     * @return badge
     */
    @Autowired
    public DigitalBadge getBadge() {
        return badge;
    }

    @Autowired
    private DigitalBadge badge;
    @Autowired
    private DirectAccessBadgeWalletDAO dao;
    @Autowired
    List<DigitalBadge> tableList;

    @Autowired
    AddBadgeDialog dialog;

    /**
     * aggrégation vers objet BadgeWalletController
     */
    @Autowired
    BadgeWalletController badgeWalletController;

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    /**
     * Constructeur
     * Commentez-moi et décrivez le méchanisme de Forms
     */
    public BadgeWalletGUI() {
        dialog = new AddBadgeDialog();
        dialog.setCaller(this);
        dialog.setDao(dao);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.pack();
                dialog.setLocationRelativeTo(null);
                dialog.setIconImage(dialog.getIcon().getImage());
                dialog.setVisible(true);
            }
        });

        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());
        table1.setModel(tableModel);
        table1.setRowSelectionInterval(0, 0);

        // 2. Les Renderers...
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    loadBadge(row);
                }
            }
        });


    }

    /**
     * Commentez-moi
     * @param args
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet");
        BadgeWalletGUI gui = new BadgeWalletGUI();
        frame.setContentPane(gui.panelParent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(gui.getIcon().getImage());
        frame.setVisible(true);
    }

    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

/*    private void createUIComponents() {
        try {
            this.dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            // 1. Le Model
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
            this.tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            // Badge initial
            this.badge = tableList.get(0);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        this.setCreatedUIFields();

    }*/

    /**
     * creation des composans graphique via Delegate
     */
    private void createUIComponents() {
        this.badgesWalletController = getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);

        badgesWalletController.delegateUIComponentsCreation(this);
        badgesWalletController.delegateUIManagedFieldsCreation(this);
    }

    /**
     * Commentez-moi
     */
    private void setCreatedUIFields() {
        this.badgePanel = new BadgePanel(badge, dao);
        this.badgePanel.setPreferredSize(new Dimension(256, 256));
        this.panelImageContainer = new JPanel();
        this.panelImageContainer.add(badgePanel);
    }


    /**
     * Commentez-moi
     * @param badge
     */
    public void setAddedBadge(DigitalBadge badge) {
        this.badge = badge;
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1);
    }

    /**
     * Commentez-moi
     * @param row
     */
    private void loadBadge(int row){
        panelHaut.removeAll();
        panelHaut.revalidate();

        badge = tableList.get(row);
        setCreatedUIFields();
        table1.setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    }

    /**
     * getter qui retourne info du PanelParent
     * @return
     */
    public Container getPanelParent() {
        return this.panelParent;
    }

    /**
     * getter de l'instance ?
     * @return
     */
    private Dialog getInstance() {
         return this.dialog ;
        // TODO: 16/12/2022  
    }
}
